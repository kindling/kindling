@php
// Template Name: Shortcode Examples
@endphp

@extends('layout.base')

@section('content')
    <div class="container flex flex-wrap">
        <h2 class="w-full mb-8">What You Type:</h2>
        <div class="flex-1 px-4">
            <h3>2 Columns</h3>
            @php
            $rowOpen = "[row]\n";
            $rowClose = "\n[/row]";
            $twoColumns = "{$rowOpen}[col-half]\nColumn 1\n[/col-half]\n\n";
            $twoColumns .= "[col-half]\nColumn 2\n[/col-half]{$rowClose}";
            echo '<pre>' . str_replace('[', '<span>[</span>', $twoColumns) . '</pre>';
            @endphp
        </div>
        <div class="flex-1 px-4">
            <h3>3 Columns</h3>
            @php
            $rowOpen = "[row]\n";
            $rowClose = "\n[/row]";
            $threeColumns = "{$rowOpen}[col-third]\nColumn 1\n[/col-third]\n\n";
            $threeColumns .= "[col-third]\nColumn 2\n[/col-third]\n\n";
            $threeColumns .= "[col-third]\nColumn 3\n[/col-third]{$rowClose}";
            echo '<pre>' . str_replace('[', '<span>[</span>', $threeColumns) . '</pre>';
            @endphp
        </div>

        <div class="flex-1 px-4">
            <h3>4 Columns</h3>
            @php
            $rowOpen = "[row]\n";
            $rowClose = "\n[/row]";
            $fourColumns = "{$rowOpen}[col-quarter]\nColumn 1\n[/col-quarter]\n\n";
            $fourColumns .= "[col-quarter]\nColumn 2\n[/col-quarter]\n\n";
            $fourColumns .= "[col-quarter]\nColumn 3\n[/col-quarter]\n\n";
            $fourColumns .= "[col-quarter]\nColumn 4\n[/col-quarter]{$rowClose}";
            echo '<pre>' . str_replace('[', '<span>[</span>', $fourColumns) . '</pre>';
            @endphp
        </div>


        <div class="flex-1 px-4">
            <h3>Button</h3>

            [button link="#link" new-tab="1" label="Button Text Here"]
            <br />
            <br />
            <ul>
                <li>Requires link and label</li>
                <li>new-tab is optional, give it a value of 1 to open link in new tab</li>
            </ul>
        </div>
        <div class="w-full my-8">
            <span class="h2">PRO TIP: Always wrap your column shortcodes with a row shortcode!</span>
        </div>

    </div>
    <div class="container my-16">
        <h2>Edit this page to test these shortcodes in the content editor and view your results here:</h2>
        @include('content.loop')
    </div>
@endsection

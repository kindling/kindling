@php
// Template Name: Screen Size Test
@endphp

@extends('layout.base')

@section('content')
    <div class="container">
        <h1 class="mb-8">Screen Size Test</h1>
         <div class="flex mb-8">
            <div class="text-xs text-grey m-1 p-1">Active Screen Size:</div>
            <div class="p-1 text-xs m-1 bg-grey text-white hidden xxl:block xxl:bg-success"><i class="fas fa-check"></i> screen-xxl</div>
            <div class="p-1 text-xs m-1 bg-grey xl:bg-success xxl:bg-grey text-white hidden xl:block"><i class="fas fa-check"></i> screen-xl</div>
            <div class="p-1 text-xs m-1 bg-grey lg:bg-success xl:bg-grey text-white hidden lg:block"><i class="fas fa-check"></i> screen-lg</div>
            <div class="p-1 text-xs m-1 bg-grey md:bg-success lg:bg-grey text-white hidden md:block"><i class="fas fa-check"></i> screen-md</div>
            <div class="p-1 text-xs m-1 bg-grey sm:bg-success md:bg-grey text-white hidden sm:block"><i class="fas fa-check"></i> screen-sm</div>
            <div class="p-1 text-xs m-1 bg-success sm:bg-grey text-white"><i class="fas fa-check"></i> Mobile <span class="sm:hidden">Only</span></div>
        </div>
    </div>
    <div class="bg-theme-1 relative py-8 my-8">
        <div id="container" class="container mb-8 text-center text-white text-xl">
            <p>This area is the width of the browser window.</p>
            <p>Background images need to be large enough to fill this area on large screens.</p>
        </div>
        <div class="container bg-grey text-black h-64 flex flex-col justify-center text-xl text-center">
            <p class="">This grey area is the width of the ".container" class.</p>
            <p>This class controls the width of content on every page.</p>
            <p>Right now this container is:</p>
            <p class="text-5xl font-bold">
                <span class="hidden xl:block">1200px wide</span>
                <span class="hidden lg:block xl:hidden">992px wide</span>
                <span class="hidden md:block lg:hidden">768px wide</span>
                <span class="hidden sm:block md:hidden">576px wide</span>
                <span class="sm:hidden">100% width</span>
            </p>
        </div>
    </div>

     <div class="bg-theme-1 p-8 mx-auto text-white flex items-center justify-center text-3xl mb-16" style="width:375px;height:812px;">
        375x812 <br>
        iPhone X screen in points
    </div>

    <div class="bg-theme-1 p-8 mx-auto text-white flex items-center justify-center text-3xl mb-16" style="width:1200px;height:500px;">
        1200x500 <br>
        Default max width of the .container kindling class
    </div>

    <div class="bg-theme-1 p-8 mx-auto text-white flex items-center justify-center text-3xl mb-16" style="width:1440px;height:900px;">
        1440x900 <br>
        Resolution of a macbook screen scaled to "Default"
    </div>

    <div class="bg-theme-1 p-8 mx-auto text-white flex items-center justify-center text-3xl mb-16" style="width:1920px;height:1080px;">
        1920x1080 <br>
        Resolution of a macbook screen scaled to "More Space"
    </div>
@endsection

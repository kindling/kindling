import debounce from "./debounce";

let navigation = {
    $itemToggle: undefined,
    $menuToggle: undefined,
    $toggleText: undefined,
    $menu: undefined,
    debounce: debounce.new(),

    menuToggleInit() {
        navigation.$menuToggle.on("click", function(e) {
            e.preventDefault();
            $(this).toggleClass("open");
            navigation.$menu.toggleClass("open");
            navigation.$toggleText.text('Close');
            if (!navigation.$menu.hasClass("open")) {
                navigation.$menu.find(".dropdown").removeClass("open");
                navigation.$toggleText.text('Menu');
            }
        });
    },

    itemToggleInit() {
        navigation.$itemToggle.on("click", function(e) {
            e.preventDefault();
            $(this)
                .parent(".dropdown")
                .toggleClass("open");
        });
        if( $(navigation.$itemToggle).is(':visible') ) {
            navigation.$itemToggle.parent('.dropdown').children('a[href="#"]').on("click", function(e){
                e.preventDefault();
                $(this)
                .parent(".dropdown")
                .toggleClass("open");
            });//links with a hash toggle their sub menu
        }
    },

    handleResize() {
        if (navigation.$itemToggle.css("display") === "none") {
            $(".dropdown.open").removeClass("open");
        }
    }
};

export default function() {
    navigation.$itemToggle = $(".js-item-toggle");
    navigation.$menu = $(".js-navigation-menu");
    navigation.$menuToggle = $(".js-menu-toggle");
    navigation.$toggleText = $(".js-menu-toggle-text");

    navigation.itemToggleInit();
    navigation.menuToggleInit();
    $(document).ready(function(){
        $(navigation.$itemToggle.parent('.dropdown').children('a[href="#"]')).addClass('cursor-default');
    });
    $(window).resize(function() {
        navigation.debounce.set(navigation.handleResize, 500);
    });
}

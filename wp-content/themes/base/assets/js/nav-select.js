export default function() {

    $('.js-nav-select').on('change',function(){
        var url = $(this).val();
        if(url){
            window.location.href = url;
        }
    });

}

<div class="js-menu-toggle menu-toggle">
    <div class="toggle-text-wrap js-menu-toggle-text">
        <span>Menu</span>
    </div>

    <div class="toggle-span-wrap">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>

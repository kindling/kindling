@if (has_nav_menu('blog_navigation'))
    <div class="categories">

        <h2>Categories</h2>

        @php
            //get any menu in the blog navigation menu position and loop over it
            $menu_location = get_nav_menu_locations()['blog_navigation'];
            $menu_object = wp_get_nav_menu_object($menu_location);
            $items = wp_get_nav_menu_items( $menu_object );
            $active_cat = get_queried_object()->name;
        @endphp

        <div class="custom-select">
            <select class="js-nav-select">
                @foreach($items as $item)
                    <option value="{{$item->url}}" {{ $item->title === $active_cat ? 'selected' : '' }} >{{$item->title}}</option>
                @endforeach
            </select>
        </div>


        @php
        wp_nav_menu([
        'theme_location' => 'blog_navigation',
        'menu_class' => 'blog-navigation-menu',
        'container' => 'nav',
        'container_class' => 'blog-navigation',
        'walker' => new \Kindling\Support\NavWalkers\PrimaryNavWalker
        ]);
        @endphp
    </div>

@endif

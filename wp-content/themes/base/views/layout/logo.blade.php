<div class="logo-wrap">
    <a class="logo" href="{{ home_url() }}">
        <img src="{{ kindling_image_path('logo.png') }}" alt="{{ bloginfo('name') }}">
    </a>
</div>

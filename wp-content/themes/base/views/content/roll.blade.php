@php
    $roll_id =  get_option('page_for_posts');
    $roll_title = get_field('title_override', $roll_id) ?? get_the_title($roll_id);
@endphp
<div class="container">
    <h1>
        {{$roll_title}}
    </h1>
    <div class="content-roll">
        @include('layout.blog-navigation')
        @while(have_posts())
            {{ the_post() }}
            @include('content.roll-item')
        @endwhile
    </div>
    @include('partials.pagination')
</div>

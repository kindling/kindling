<div class="roll-item mb-8">
    <a href="{{ get_post_permalink() }}">
        {{ the_post_thumbnail('blog-thumbnail') }}
    </a>

    <a href="{{ get_post_permalink() }}" class="h2 roll-item-title">{{ the_title() }}</a>

    <div class="flex mb-4">

        <span class="block mr-4">{{get_the_date()}}</span>

        {{the_category(',&nbsp;')}}
    </div>

    <div class="roll-item-content">
        {{ the_excerpt() }}
    </div>



    <a href="{{ get_post_permalink() }}">Read more &raquo;</a>
</div>

@extends('layout.base')

@section('content')
    @include('layout.page-header')
    <div class="container">
        <div class="flex mb-4">

            <div class="mr-4">{{get_the_date()}}</div>

            {{the_category(',&nbsp;')}}

        </div>
        @include('content.loop')

        <a class="inline-block mt-8 mb-16"  href="{{ get_post_type_archive_link( 'post' ) }}">
            <i class="fas fa-arrow-left mr-2"></i>Back to Blog Home
        </a>
    </div>
@endsection

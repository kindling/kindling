<?php
/**
 * Theme Shortcodes.
 *
 * @package Kindling_Theme
 * @author  Matchbox Design Group <info@matchboxdesigngroup.com>
 */


function kindling_button($button){
    $button = '<a class="btn" target="' . ($button['new-tab'] === '1' ? '_blank' : '_self') . '" href="' . $button['link'] . '"><span>' . $button['label'] . ' </span><span class="icon"> <i class="fas fa-arrow-right"></i> </span><span class="bg"></span></a>';
    return $button;
}
add_shortcode('button','kindling_button');


function kindling_col_quarter($output,$content = null){
    $output = '<div class="w-full sm:w-1/2 lg:w-1/4 px-4 pb-1 mb-4">' . $content . '</div>';
    return $output;
}
add_shortcode('col-quarter','kindling_col_quarter');

function kindling_col_third($output,$content = null){
    $output = '<div class="w-full sm:w-1/2 md:w-1/3 px-4 pb-1 mb-4">' . $content . '</div>';
    return $output;
}
add_shortcode('col-third','kindling_col_third');

function kindling_col_half($output,$content = null){
    $output = '<div class="w-full md:w-1/2 px-4 pb-1 mb-4">' . $content . '</div>';
    return $output;
}
add_shortcode('col-half','kindling_col_half');



function kindling_row($output, $content = null){

    $output =  '<div class="row my-4 flex flex-wrap items-start justify-start w-full-plus-8 -mx-4">' . do_shortcode($content) . '</div>';


    return $output;
}
add_shortcode('row','kindling_row');

